package me.derenderkeks.randompaperutils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class CommandHandler implements TabExecutor {
    private static final HashMap<String, TabExecutor> commands = new HashMap<>();

    public CommandHandler() {
        register("help", new CommandHelp());
        register("give", new CommandGive());
    }

    public void register(String name, TabExecutor handler) {
        commands.put(name, handler);
    }

    public boolean exists(String name) {
        return commands.containsKey(name);
    }

    public TabExecutor getHandler(String name) {
        return commands.get(name);
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        String cmd = "help";

        if (args.length > 0) {
            cmd = args[0];
        }

        if (!exists(cmd)) {
            sender.sendMessage(ChatColor.DARK_RED + "Invalid command.");
            return false;
        }

        String[] trimmedArgs = Arrays.copyOfRange(args, 1, args.length);
        return getHandler(cmd).onCommand(sender, command, label, trimmedArgs);
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String alias, @NotNull String[] args) {
        String cmd;

        if (args.length > 1) {
            cmd = args[0];
        } else {
            return new ArrayList<>(commands.keySet());
        }

        if (!exists(cmd)) {
            return null;
        }

        return getHandler(cmd).onTabComplete(sender, command, alias, args);
    }

    public static void sendNoPermMessage(CommandSender sender) {
        sender.sendMessage(ChatColor.DARK_RED + "You don't have permission to use this command.");
    }

    public static void sendPlayerOnlyMessage(CommandSender sender) {
        sender.sendMessage(ChatColor.DARK_RED + "This command is only usable by players.");
    }
}
