package me.derenderkeks.randompaperutils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class CommandGive implements TabExecutor {
    class GiveItemData {
        public GiveItemData(String alias, ItemStack item) {
            this.alias = alias;
            this.permission = "randompaperutils.give." + alias;
            this.item = item;
        }

        public String alias;
        public String permission;
        public ItemStack item;
    }

    private List<GiveItemData> giveItems;

    public CommandGive() {
        giveItems = new ArrayList<>();
        giveItems.add(new GiveItemData("molotov", MolotovItem.getItem()));
        giveItems.add(new GiveItemData("tpstaff", TPStaffItem.getItem()));
        giveItems.add(new GiveItemData("eggnade_lite", EggnadeItem.getItem((byte) 1)));
        giveItems.add(new GiveItemData("eggnade", EggnadeItem.getItem((byte) 0)));
        giveItems.add(new GiveItemData("eggnade_pro", EggnadeItem.getItem((byte) 2)));
        giveItems.add(new GiveItemData("eggnade_dislocator", EggnadeItem.getItem((byte) 3)));
        giveItems.add(new GiveItemData("party_potion", PartyPotionItem.getItem()));
        giveItems.add(new GiveItemData("party_egg", PartyEggItem.getItem()));
        giveItems.add(new GiveItemData("milk_potion", MilkPotionItem.getItem()));
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (!(sender instanceof Player player)) {
            CommandHandler.sendPlayerOnlyMessage(sender);
            return false;
        }
        if (args.length == 0) {
            sender.sendMessage(ChatColor.DARK_RED + "Item name is required.");
            return false;
        }
        String item = args[0];
        Optional<GiveItemData> optionalGiveItemData = giveItems.stream().filter(giveItemData -> giveItemData.alias.equals(item)).findFirst();
        if (optionalGiveItemData.isEmpty()) {
            sender.sendMessage(ChatColor.DARK_RED + "Invalid item name.");
            return false;
        }
        GiveItemData giveData = optionalGiveItemData.get();
        if (!sender.hasPermission(giveData.permission)) {
            CommandHandler.sendNoPermMessage(sender);
            return false;
        }
        player.getInventory().addItem(giveData.item.clone());
        return true;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String alias, @NotNull String[] args) {
        if (!(sender instanceof Player)) {
            return Collections.emptyList();
        }
        return giveItems.stream().filter(giveItemData -> sender.hasPermission(giveItemData.permission)).map(giveItemData -> giveItemData.alias).toList();
    }
}
