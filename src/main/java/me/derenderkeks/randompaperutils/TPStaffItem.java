package me.derenderkeks.randompaperutils;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

public class TPStaffItem {
    public static ItemStack getItem() {
        ItemStack staff = new ItemStack(Material.BLAZE_ROD);
        ItemMeta staffMeta = staff.getItemMeta();
        staffMeta.displayName(Component.text("Teleport Staff")
                .color(TextColor.color(0x0b5750))
                .decoration(TextDecoration.ITALIC, false));
        staffMeta.getPersistentDataContainer().set(NamespaceKeys.TPSTAFF.get(), PersistentDataType.BYTE, (byte) 1);
        staffMeta.addEnchant(Enchantment.MENDING, 1, false);
        staffMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        staff.setItemMeta(staffMeta);
        return staff;
    }
}
