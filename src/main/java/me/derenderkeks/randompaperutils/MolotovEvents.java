package me.derenderkeks.randompaperutils;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.projectiles.BlockProjectileSource;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.ThreadLocalRandom;

public class MolotovEvents implements Listener {

    @EventHandler(ignoreCancelled = true)
    public void onMolotovImpact(PotionSplashEvent event) {
        if (!isMolotovItem(event.getEntity().getItem())) {
            return;
        }
        event.setCancelled(true);

        if (!(event.getPotion().getShooter() instanceof Entity || event.getPotion().getShooter() instanceof BlockProjectileSource)) {
            return;
        }

        int width = 10;
        int height = 5;
        Location baseLocation = event.getEntity().getLocation();
        if (baseLocation.getBlock().isLiquid()) {
            return;
        }

        for (Entity entity : event.getAffectedEntities()) {
            entity.setFireTicks(20 * 10);
        }

        // ignite 0.3 of a width² area, up to height-1 deep and one above
        baseLocation.subtract((float) width / 2, height - 1, (float) width / 2);
        ThreadLocalRandom random = ThreadLocalRandom.current();

        for (int y = 0; y <= height; y++) {
            for (int x = 0; x <= width; x++) {
                for (int z = 0; z <= width; z++) {
                    Location tmpLocation = baseLocation.clone().add(x, y, z);
                    Block block = tmpLocation.getBlock();
                    if (!block.getType().isAir()) {
                        continue;
                    }
                    if (!tmpLocation.clone().subtract(0, 1, 0).getBlock().getType().isSolid()) {
                        continue;
                    }
                    if (random.nextInt(3) != 0) {
                        continue;
                    }

                    BlockIgniteEvent blockIgniteEvent;
                    if (event.getPotion().getShooter() instanceof Entity shooterEntity) {
                        blockIgniteEvent = new BlockIgniteEvent(block, BlockIgniteEvent.IgniteCause.FIREBALL, shooterEntity);
                    } else if (event.getPotion().getShooter() instanceof BlockProjectileSource shooterBlock) {
                        blockIgniteEvent = new BlockIgniteEvent(block, BlockIgniteEvent.IgniteCause.FIREBALL, shooterBlock.getBlock());
                    } else {
                        continue;
                    }

                    Bukkit.getPluginManager().callEvent(blockIgniteEvent);
                    if (blockIgniteEvent.isCancelled()) {
                        continue;
                    }

                    block.setType(Material.FIRE);
                }
            }
        }
    }

    private boolean isMolotovItem(@Nullable ItemStack item) {
        return item != null
                && item.getType() == Material.SPLASH_POTION
                && item.getItemMeta().getPersistentDataContainer().has(NamespaceKeys.MOLOTOV.get(), PersistentDataType.BYTE);
    }
}
