package me.derenderkeks.randompaperutils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class CommandHelp implements TabExecutor {
    private HashMap<String, String> helpEntries;

    public CommandHelp() {
        helpEntries = new HashMap<>();
        helpEntries.put("help", "Display this usage");
        helpEntries.put("give <item>", "Obtain one of the special items");
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        sender.sendMessage(ChatColor.GOLD + "Usage of " + ChatColor.AQUA + "/rpu" + ChatColor.GOLD + ":\n");
        helpEntries.forEach((c, d) -> sendHelpEntry(sender, c, d));
        return true;
    }

    private void sendHelpEntry(CommandSender sender, String cmd, String desc) {
        sender.sendMessage(ChatColor.AQUA + cmd + ChatColor.GRAY + " - " + ChatColor.GOLD + desc);
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String alias, @NotNull String[] args) {
        return Collections.emptyList();
    }
}
