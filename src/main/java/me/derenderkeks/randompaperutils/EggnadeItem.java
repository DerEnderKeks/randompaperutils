package me.derenderkeks.randompaperutils;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import java.util.Collections;

public class EggnadeItem {
    public static void registerRecipes() {
        for (byte i = 0; i <= 3; i++) {
            registerRecipe(i);
        }
    }

    private static void registerRecipe(byte type) {
        ItemStack egg = getItem(type);

        ShapedRecipe recipe;
        switch (type) {
            case 1 -> {
                recipe = new ShapedRecipe(NamespaceKeys.EGGNADE_LITE.get(), egg);
                recipe.shape("GFG", "SES", "GGG");
                recipe.setIngredient('F', Material.FEATHER);
                recipe.setIngredient('G', Material.GUNPOWDER);
            }
            case 2 -> {
                recipe = new ShapedRecipe(NamespaceKeys.EGGNADE_PRO.get(), egg);
                recipe.shape("TNT", "SES", "TTT");
                recipe.setIngredient('T', Material.TNT);
                recipe.setIngredient('N', Material.NETHER_STAR);
            }
            case 3 -> {
                recipe = new ShapedRecipe(NamespaceKeys.EGGNADE_DISLOCATOR.get(), egg);
                recipe.shape("PPP", "SES", "PPP");
                recipe.setIngredient('P', Material.ENDER_PEARL);
            }
            default -> {
                recipe = new ShapedRecipe(NamespaceKeys.EGGNADE.get(), egg);
                recipe.shape("GGG", "SES", "GGG");
                recipe.setIngredient('G', Material.GUNPOWDER);
            }
        }
        recipe.setIngredient('S', Material.SAND);
        recipe.setIngredient('E', Material.EGG);

        Bukkit.addRecipe(recipe);
    }

    public static ItemStack getItem(byte type) {
        ItemStack egg = new ItemStack(Material.EGG);
        ItemMeta eggMeta = egg.getItemMeta();
        Component eggName = Component.text("Eggnade", TextColor.color(0x6100ab)).decoration(TextDecoration.ITALIC, false);

        switch (type) {
            case 1 -> {
                eggName = eggName.append(Component.text(" Lite", NamedTextColor.BLUE).decoration(TextDecoration.ITALIC, true));
                eggMeta.lore(Collections.singletonList(Component.text("No block damage", NamedTextColor.DARK_AQUA)
                        .decoration(TextDecoration.ITALIC, false)));
                eggMeta.getPersistentDataContainer().set(NamespaceKeys.EGGNADE_LITE.get(), PersistentDataType.BYTE, (byte) 1);
            }
            case 2 -> {
                eggName = eggName.append(Component.text(" Pro", NamedTextColor.DARK_RED).decoration(TextDecoration.BOLD, true));
                eggMeta.lore(Collections.singletonList(Component.text("Extreme", NamedTextColor.DARK_RED)
                        .append(Component.text(" block damage", NamedTextColor.DARK_AQUA))
                        .decoration(TextDecoration.ITALIC, false)));
                eggMeta.getPersistentDataContainer().set(NamespaceKeys.EGGNADE_PRO.get(), PersistentDataType.BYTE, (byte) 1);
            }
            case 3 -> {
                eggName = eggName.append(Component.text(" Dislocator", TextColor.color(0x0b5750)));
                eggMeta.lore(Collections.singletonList(Component.text("Dislocates blocks", NamedTextColor.DARK_AQUA)
                        .decoration(TextDecoration.ITALIC, false)));
                eggMeta.getPersistentDataContainer().set(NamespaceKeys.EGGNADE_DISLOCATOR.get(), PersistentDataType.BYTE, (byte) 1);
            }
            default -> {
                eggMeta.lore(Collections.singletonList(Component.text("Block damage", NamedTextColor.DARK_AQUA)
                        .decoration(TextDecoration.ITALIC, false)));
                eggMeta.getPersistentDataContainer().set(NamespaceKeys.EGGNADE.get(), PersistentDataType.BYTE, (byte) 1);
            }
        }
        eggMeta.displayName(eggName);

        eggMeta.addEnchant(Enchantment.MENDING, 1, false);
        eggMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        egg.setItemMeta(eggMeta);
        return egg;
    }
}
