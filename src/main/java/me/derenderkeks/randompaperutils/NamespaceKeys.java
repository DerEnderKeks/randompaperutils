package me.derenderkeks.randompaperutils;

import org.bukkit.NamespacedKey;

/*public class NamespaceKeys {
    public static final NamespacedKey tpStaffKey = new NamespacedKey(RandomPaperUtils.getPlugin(RandomPaperUtils.class), "tpStaff");
    public static final NamespacedKey tpStaffLastUsedKey = new NamespacedKey(RandomPaperUtils.getPlugin(RandomPaperUtils.class), "tpStaffLastUsed");
    public static final NamespacedKey molotovKey = new NamespacedKey(RandomPaperUtils.getPlugin(RandomPaperUtils.class), "molotov");
    public static final NamespacedKey partyEggKey = new NamespacedKey(RandomPaperUtils.getPlugin(RandomPaperUtils.class), "partyEgg");
    public static final NamespacedKey eggnadeKey = new NamespacedKey(RandomPaperUtils.getPlugin(RandomPaperUtils.class), "eggnade");
    public static final NamespacedKey eggnadeLiteKey = new NamespacedKey(RandomPaperUtils.getPlugin(RandomPaperUtils.class), "eggnadeLite");
    public static final NamespacedKey eggnadeProKey = new NamespacedKey(RandomPaperUtils.getPlugin(RandomPaperUtils.class), "eggnadePro");
    public static final NamespacedKey eggnadeDislocatorKey = new NamespacedKey(RandomPaperUtils.getPlugin(RandomPaperUtils.class), "eggnadeDislocator");
}*/
public enum NamespaceKeys {
    TPSTAFF(new NamespacedKey(RandomPaperUtils.getPlugin(RandomPaperUtils.class), "tpStaff")),
    TPSTAFF_LAST_USED(new NamespacedKey(RandomPaperUtils.getPlugin(RandomPaperUtils.class), "tpStaffLastUsed")),
    MOLOTOV(new NamespacedKey(RandomPaperUtils.getPlugin(RandomPaperUtils.class), "molotov")),
    PARTY_EGG(new NamespacedKey(RandomPaperUtils.getPlugin(RandomPaperUtils.class), "partyEgg")),
    PARTY_POTION(new NamespacedKey(RandomPaperUtils.getPlugin(RandomPaperUtils.class), "partyPotion")),
    EGGNADE(new NamespacedKey(RandomPaperUtils.getPlugin(RandomPaperUtils.class), "eggnade")),
    EGGNADE_LITE(new NamespacedKey(RandomPaperUtils.getPlugin(RandomPaperUtils.class), "eggnadeLite")),
    EGGNADE_PRO(new NamespacedKey(RandomPaperUtils.getPlugin(RandomPaperUtils.class), "eggnadePro")),
    EGGNADE_DISLOCATOR(new NamespacedKey(RandomPaperUtils.getPlugin(RandomPaperUtils.class), "eggnadeDislocator")),
    MILK_POTION(new NamespacedKey(RandomPaperUtils.getPlugin(RandomPaperUtils.class), "milkPotion"));

    private final NamespacedKey key;

    NamespaceKeys(NamespacedKey key) {
        this.key = key;
    }

    public NamespacedKey get() {
        return key;
    }
}