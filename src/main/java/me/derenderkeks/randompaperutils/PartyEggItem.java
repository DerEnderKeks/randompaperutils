package me.derenderkeks.randompaperutils;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextDecoration;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

public class PartyEggItem {
    public static ItemStack getItem() {
        ItemStack egg = new ItemStack(Material.EGG);
        ItemMeta eggMeta = egg.getItemMeta();
        Component eggName = LegacyComponentSerializer.legacyAmpersand().deserialize("&aP&6a&4r&9t&3y &6E&bg&ag")
                .decoration(TextDecoration.ITALIC, false);
        eggMeta.displayName(eggName);
        eggMeta.getPersistentDataContainer().set(NamespaceKeys.PARTY_EGG.get(), PersistentDataType.BYTE, (byte) 1);
        eggMeta.addEnchant(Enchantment.MENDING, 1, false);
        eggMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        egg.setItemMeta(eggMeta);
        return egg;
    }
}
