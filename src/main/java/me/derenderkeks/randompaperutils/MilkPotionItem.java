package me.derenderkeks.randompaperutils;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

import java.util.Collections;

public class MilkPotionItem {
    public static ItemStack getItem() {
        ItemStack potion = new ItemStack(Material.SPLASH_POTION);
        PotionMeta potionMeta = (PotionMeta) potion.getItemMeta();
        Component potionName = LegacyComponentSerializer.legacyAmpersand().deserialize("&fMilk Potion")
                .decoration(TextDecoration.ITALIC, false);

        potionMeta.lore(Collections.singletonList(Component.text("Like a milk bucket, but throwable!", NamedTextColor.WHITE)
                .decoration(TextDecoration.ITALIC, false)));
        potionMeta.getPersistentDataContainer().set(NamespaceKeys.MILK_POTION.get(), PersistentDataType.BYTE, (byte) 1);
        potionMeta.displayName(potionName);

        potionMeta.addEnchant(Enchantment.MENDING, 1, false);
        potionMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        potionMeta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        potionMeta.setBasePotionData(new PotionData(PotionType.SLOW_FALLING));
        potion.setItemMeta(potionMeta);
        return potion;
    }

    public static void registerRecipe() {
        ItemStack potion = getItem();

        ShapelessRecipe recipe;
        recipe = new ShapelessRecipe(NamespaceKeys.MILK_POTION.get(), potion);
        recipe.addIngredient(Material.SPLASH_POTION);
        recipe.addIngredient(Material.MILK_BUCKET);

        Bukkit.addRecipe(recipe);
    }
}
