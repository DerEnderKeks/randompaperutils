package me.derenderkeks.randompaperutils;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

import java.util.Collections;

public class PartyPotionItem {
    public static ItemStack getItem() {
        ItemStack potion = new ItemStack(Material.SPLASH_POTION);
        PotionMeta potionMeta = (PotionMeta) potion.getItemMeta();
        Component potionName = LegacyComponentSerializer.legacyAmpersand().deserialize("&aP&6a&4r&9t&3y &6P&bo&at&4i&9o&3n")
                .decoration(TextDecoration.ITALIC, false);

        potionMeta.lore(Collections.singletonList(Component.text("Chicken love it", NamedTextColor.DARK_AQUA)
                .decoration(TextDecoration.ITALIC, false)));
        potionMeta.getPersistentDataContainer().set(NamespaceKeys.PARTY_POTION.get(), PersistentDataType.BYTE, (byte) 1);
        potionMeta.displayName(potionName);

        potionMeta.addEnchant(Enchantment.MENDING, 1, false);
        potionMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        potionMeta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        potionMeta.setBasePotionData(new PotionData(PotionType.JUMP));
        potion.setItemMeta(potionMeta);
        return potion;
    }

    public static void registerRecipe() {
        ItemStack potion = getItem();

        ShapedRecipe recipe;
        recipe = new ShapedRecipe(NamespaceKeys.PARTY_EGG.get(), potion);
        recipe.shape("CCC", "FPF", "CCC");
        recipe.setIngredient('C', Material.CAKE);
        recipe.setIngredient('P', Material.SPLASH_POTION);
        recipe.setIngredient('F', Material.FIREWORK_ROCKET);

        Bukkit.addRecipe(recipe);
    }
}
