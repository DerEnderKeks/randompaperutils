package me.derenderkeks.randompaperutils;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

public class MolotovItem {
    public static void registerRecipe() {
        ItemStack molotov = getItem();

        ShapedRecipe recipe = new ShapedRecipe(NamespaceKeys.MOLOTOV.get(), molotov);
        recipe.shape(" B ", "BPB", " B ");
        recipe.setIngredient('P', Material.SPLASH_POTION);
        recipe.setIngredient('B', Material.BLAZE_POWDER);

        Bukkit.addRecipe(recipe);
    }

    public static ItemStack getItem() {
        ItemStack molotov = new ItemStack(Material.SPLASH_POTION);
        PotionMeta molotovMeta = (PotionMeta) molotov.getItemMeta();

        molotovMeta.displayName(Component.text("Molotov cocktail", TextColor.color(0xeb4034))
                .decoration(TextDecoration.ITALIC, false));
        molotovMeta.getPersistentDataContainer().set(NamespaceKeys.MOLOTOV.get(), PersistentDataType.BYTE, (byte) 1);
        molotovMeta.addEnchant(Enchantment.MENDING, 1, false);
        molotovMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        molotovMeta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        molotovMeta.setBasePotionData(new PotionData(PotionType.FIRE_RESISTANCE));
        molotov.setItemMeta(molotovMeta);
        return molotov;
    }
}
