package me.derenderkeks.randompaperutils;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.persistence.PersistentDataType;

public class ItemCraftEvents implements Listener {
    @EventHandler(ignoreCancelled = true)
    public void onCraft(CraftItemEvent event) {
        for (NamespaceKeys myKey : NamespaceKeys.values()) {
            if (!event.getRecipe().getResult().getItemMeta().getPersistentDataContainer().has(myKey.get(), PersistentDataType.BYTE)) {
                continue;
            }
            String permission;
            switch (myKey) {
                case EGGNADE_LITE -> permission = "eggnade";
                case EGGNADE -> permission = "eggnade_lite";
                case EGGNADE_PRO -> permission = "eggnade_pro";
                case EGGNADE_DISLOCATOR -> permission = "eggnade_dislocator";
                case MOLOTOV -> permission = "molotov";
                case PARTY_POTION -> permission = "party_potion";
                case MILK_POTION -> permission = "milk_potion";
                default -> permission = "";
            }
            if (permission.equals("")) {
                return;
            }
            permission = "randompaperutils.craft." + permission;
            if (!event.getWhoClicked().hasPermission(permission)) {
                event.setCancelled(true);
                event.getWhoClicked().sendActionBar(Component.text("You are not allowed to craft this item!", NamedTextColor.DARK_RED));
            }
        }
    }
}
