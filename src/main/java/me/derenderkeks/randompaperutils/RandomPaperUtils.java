package me.derenderkeks.randompaperutils;

import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;

public final class RandomPaperUtils extends JavaPlugin {

    @Override
    public void onEnable() {
        Objects.requireNonNull(this.getCommand("rpu")).setExecutor(new CommandHandler());

        getServer().getPluginManager().registerEvents(new TPStaffEvents(), this);
        getServer().getPluginManager().registerEvents(new MolotovEvents(), this);
        getServer().getPluginManager().registerEvents(new PartyEggEvents(), this);
        getServer().getPluginManager().registerEvents(new EggnadeEvents(), this);
        getServer().getPluginManager().registerEvents(new ItemCraftEvents(), this);
        getServer().getPluginManager().registerEvents(new MilkPotionEvents(), this);

        MolotovItem.registerRecipe();
        PartyPotionItem.registerRecipe();
        EggnadeItem.registerRecipes();
        MilkPotionItem.registerRecipe();
    }

    @Override
    public void onDisable() {
    }
}
