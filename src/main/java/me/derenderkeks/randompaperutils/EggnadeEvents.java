package me.derenderkeks.randompaperutils;

import com.destroystokyo.paper.event.entity.EntityRemoveFromWorldEvent;
import com.destroystokyo.paper.event.entity.ThrownEggHatchEvent;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;

public class EggnadeEvents implements Listener {
    private final HashMap<FallingBlock, Entity> fallingBlockSourceEntityMap;

    public EggnadeEvents() {
        fallingBlockSourceEntityMap = new HashMap<>();
    }

    @EventHandler
    public void onEggnadeImpact(ThrownEggHatchEvent event) {
        byte type = getEggnadeItemType(event.getEgg().getItem());
        if (type == -1) {
            return;
        }
        event.setHatching(false);

        Location location = event.getEgg().getLocation();
        World world = location.getWorld();
        boolean blockDamage = type > 0;
        if (type == 2) {
            // create multiple explosions to create a nicer hole
            int maxIter = 3;
            int steps = 4;
            int startDist = (int) (Math.round(((double) maxIter) / 2 * steps));
            location.subtract(startDist, startDist, startDist);
            for (int i = 0; i <= maxIter; i++) {
                for (int j = 0; j <= maxIter; j++) {
                    for (int k = 0; k <= maxIter; k++) {
                        Location tmpLocation = location.clone().add(i * steps, j * steps, k * steps);
                        world.createExplosion(event.getEgg(), tmpLocation, 8f, false, true);
                    }
                }
            }
        } else if (type == 3) {
            int radius = 4;
            int radiusSqr = radius * radius;
            float velocityMultiplier = 0.25f;
            location.getWorld().playSound(location, Sound.ENTITY_GENERIC_EXPLODE, SoundCategory.BLOCKS, 1f, 1f);
            ThreadLocalRandom random = ThreadLocalRandom.current();

            Entity fakeSource = createFakeEnderman(location);

            for (int x = -radius; x <= radius; x++) {
                for (int y = radius; y >= -radius; y--) {
                    for (int z = -radius; z <= radius; z++) {
                        Location tmpLocation = location.clone().add(x, y, z);
                        if (tmpLocation.distanceSquared(location) > radiusSqr) {
                            continue;
                        }
                        Block block = tmpLocation.getBlock();
                        if (block.getType().isAir()) {
                            continue;
                        }
                        BlockData data = block.getBlockData();
                        Player player = null;
                        if (event.getEgg().getShooter() instanceof Player p) {
                            player = p;
                            BlockBreakEvent blockBreakEvent = new BlockBreakEvent(block, player);
                            blockBreakEvent.setDropItems(false);
                            blockBreakEvent.setExpToDrop(0);
                            Bukkit.getPluginManager().callEvent(blockBreakEvent);
                            if (blockBreakEvent.isCancelled()) {
                                continue;
                            }
                        } else {
                            EntityChangeBlockEvent entityChangeBlockEvent = new EntityChangeBlockEvent(
                                    fakeSource,
                                    block,
                                    Bukkit.createBlockData(Material.AIR)
                            );
                            Bukkit.getPluginManager().callEvent(entityChangeBlockEvent);
                            if (entityChangeBlockEvent.isCancelled()) {
                                continue;
                            }
                        }

                        block.setType(Material.AIR);
                        FallingBlock fallingBlock = location.getWorld().spawnFallingBlock(tmpLocation, data);
                        fallingBlockSourceEntityMap.put(fallingBlock, player == null ? fakeSource : player);
                        float distX = tmpLocation.getBlockX() - location.getBlockX() + random.nextFloat();
                        float distZ = tmpLocation.getBlockZ() - location.getBlockZ() + random.nextFloat();
                        Vector velocity = new Vector(distX, 0, distZ);
                        velocity.multiply(velocityMultiplier);
                        velocity.setY(1.2);
                        fallingBlock.getPersistentDataContainer().set(NamespaceKeys.EGGNADE_DISLOCATOR.get(), PersistentDataType.BYTE, (byte) 1);
                        fallingBlock.setVelocity(velocity);
                    }
                }
            }
            fakeSource.remove();
        } else {
            world.createExplosion(event.getEgg(), 5f, false, blockDamage);
        }
    }

    @NotNull
    private Entity createFakeEnderman(Location location) {
        Location fakeSourceLocation = location.clone();
        fakeSourceLocation.setY(99999);
        return location.getWorld().spawnEntity(fakeSourceLocation,
                EntityType.ENDERMAN,
                CreatureSpawnEvent.SpawnReason.CUSTOM,
                entity -> {
                    entity.setPersistent(false);
                    entity.setSilent(true);
                    entity.setGravity(false);
                    LivingEntity livingEntity = (LivingEntity) entity;
                    livingEntity.setAI(false);
                    livingEntity.setInvisible(true);
                    livingEntity.setCollidable(false);
                    livingEntity.setCanPickupItems(false);
                    livingEntity.setInvulnerable(true);
                });
    }

    @EventHandler
    public void onFallingBlockPlace(final EntityChangeBlockEvent event) {
        if (event.getTo().isAir() || !(event.getEntity() instanceof FallingBlock && isEggnadeDislocatorEntity(event.getEntity()))) {
            return;
        }
        Entity sourceEntity = fallingBlockSourceEntityMap.get((FallingBlock) event.getEntity());
        if (sourceEntity == null) {
            return;
        }
        final Block block = event.getBlock();
        final BlockState oldBlockState = block.getState();
        if (sourceEntity instanceof Player player) {
            // delay 1 tick to get actual new block data
            Bukkit.getScheduler().scheduleSyncDelayedTask(RandomPaperUtils.getPlugin(RandomPaperUtils.class), () -> {
                ItemStack itemStack = new ItemStack(block.getType());
                BlockPlaceEvent blockPlaceEvent = new BlockPlaceEvent(
                        block,
                        oldBlockState,
                        block.getRelative(BlockFace.DOWN),
                        itemStack,
                        player,
                        true,
                        EquipmentSlot.HAND
                );
                Bukkit.getPluginManager().callEvent(blockPlaceEvent);
            }, 1L);
        } else if (sourceEntity instanceof Enderman enderman) {
            EntityChangeBlockEvent entityChangeBlockEvent = new EntityChangeBlockEvent(
                    enderman,
                    block,
                    event.getBlockData()
            );
            Bukkit.getPluginManager().callEvent(entityChangeBlockEvent);
        }
    }

    @EventHandler
    public void fallingBlockRemoveEvent(EntityRemoveFromWorldEvent event) {
        if (!(event.getEntity() instanceof FallingBlock && isEggnadeDislocatorEntity(event.getEntity()))) {
            return;
        }
        fallingBlockSourceEntityMap.remove((FallingBlock) event.getEntity());
    }

    private boolean isEggnadeDislocatorEntity(Entity entity) {
        return entity != null
                && entity.getPersistentDataContainer().has(NamespaceKeys.EGGNADE_DISLOCATOR.get(), PersistentDataType.BYTE);
    }

    private byte getEggnadeItemType(@Nullable ItemStack item) {
        if (item == null || item.getType() != Material.EGG) {
            return -1;
        }
        PersistentDataContainer persistentDataContainer = item.getItemMeta().getPersistentDataContainer();
        if (persistentDataContainer.has(NamespaceKeys.EGGNADE_LITE.get(), PersistentDataType.BYTE)) {
            return 0;
        } else if (persistentDataContainer.has(NamespaceKeys.EGGNADE.get(), PersistentDataType.BYTE)) {
            return 1;
        } else if (persistentDataContainer.has(NamespaceKeys.EGGNADE_PRO.get(), PersistentDataType.BYTE)) {
            return 2;
        } else if (persistentDataContainer.has(NamespaceKeys.EGGNADE_DISLOCATOR.get(), PersistentDataType.BYTE)) {
            return 3;
        }
        return -1;
    }
}
