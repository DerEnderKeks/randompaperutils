package me.derenderkeks.randompaperutils;

import com.destroystokyo.paper.block.TargetBlockInfo;
import net.kyori.adventure.key.Key;
import net.kyori.adventure.sound.Sound;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class TPStaffEvents implements Listener {
    @EventHandler
    public void onTPStaffObtain(EntityPickupItemEvent event) {
        if (!(event.getEntity() instanceof Player player)) {
            return;
        }
        if (!isTPStaffItem(event.getItem().getItemStack()) || !canUseTPStaff(player)) {
            return;
        }
        showTPStaffUsage((Player) event.getEntity());
    }

    @EventHandler
    public void onTPStaffSelect(PlayerItemHeldEvent event) {
        if (!isTPStaffItem(event.getPlayer().getInventory().getItem(event.getNewSlot())) || !canUseTPStaff(event.getPlayer())) {
            return;
        }
        showTPStaffUsage(event.getPlayer());
    }

    protected static void showTPStaffUsage(Player player) {
        player.sendActionBar(
                Component.text("Left Click", NamedTextColor.DARK_AQUA)
                        .append(Component.text(": ", NamedTextColor.GRAY))
                        .append(Component.text("Teleport to furthest valid space", NamedTextColor.GOLD))
                        .append(Component.text(" - ", NamedTextColor.GRAY))
                        .append(Component.text("Right Click", NamedTextColor.DARK_AQUA))
                        .append(Component.text(": ", NamedTextColor.GRAY))
                        .append(Component.text("Teleport to nearest valid space", NamedTextColor.GOLD))
        );
    }

    @EventHandler
    public void onTPStaffUse(PlayerInteractEvent event) {
        if (event.getAction() == Action.PHYSICAL
                || event.getAction() == Action.LEFT_CLICK_AIR
                || !isTPStaffItem(event.getItem())
                || !canUseTPStaff(event.getPlayer())
        ) {
            return;
        }
        event.setCancelled(true);

        Player player = event.getPlayer();
        Location location = player.getLocation();
        TargetBlockInfo targetBlockInfo = player.getTargetBlockInfo(16);

        if (targetBlockInfo == null) {
            return;
        }

        ItemStack staff = event.getItem();
        ItemMeta staffMeta = staff.getItemMeta();

        if (staffMeta.getPersistentDataContainer().has(NamespaceKeys.TPSTAFF_LAST_USED.get(), PersistentDataType.LONG)
                && Optional.ofNullable(staffMeta.getPersistentDataContainer().get(NamespaceKeys.TPSTAFF_LAST_USED.get(), PersistentDataType.LONG))
                .orElse(0L) + 100 > System.currentTimeMillis()
        ) {
            return;
        }

        staffMeta.getPersistentDataContainer().set(NamespaceKeys.TPSTAFF_LAST_USED.get(), PersistentDataType.LONG, System.currentTimeMillis());
        staff.setItemMeta(staffMeta);

        if (isValidTarget(targetBlockInfo.getBlock().getType())) {
            teleportWithDirection(player, targetBlockInfo.getBlock().getLocation(), location.getPitch(), location.getYaw());
            return;
        }

        Vector direction = targetBlockInfo.getBlockFace().getOppositeFace().getDirection();
        Vector testLocVector = targetBlockInfo.getBlock().getLocation().toVector();

        // test from farthest to closes on left click
        if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
            testLocVector.add(direction.clone().multiply(16));
            direction.multiply(-1);
        }

        for (int i = 0; i < 10; i++) {
            testLocVector.add(direction);
            Location testLocation = testLocVector.toLocation(location.getWorld());
            Material targetBlockMaterial = testLocation.getBlock().getType();
            Location belowTestLocation = testLocation.clone().subtract(0, 1, 0);
            Material belowBlockMaterial = belowTestLocation.getBlock().getType();
            Location aboveTestLocation = testLocation.clone().add(0, 1, 0);
            Material aboveBlockMaterial = aboveTestLocation.getBlock().getType();

            Location tpTarget = testLocation;
            if (isValidTarget(targetBlockMaterial)) {
                if (isValidTarget(belowBlockMaterial)) {
                    tpTarget = belowTestLocation;
                } else if (!isValidTarget(aboveBlockMaterial)) {
                    continue;
                }
            } else {
                continue;
            }

            teleportWithDirection(player, tpTarget, location.getPitch(), location.getYaw());
            return;
        }
        player.sendActionBar(Component.text("Failed to find valid target location.", NamedTextColor.DARK_RED));
    }

    private void teleportWithDirection(Player player, Location target, float pitch, float yaw) {
        Location tpTarget = target.clone();
        tpTarget.setPitch(pitch);
        tpTarget.setYaw(yaw);
        tpTarget.add(.5, 0, .5);
        player.teleport(tpTarget);
        player.playSound(Sound.sound(Key.key("entity.enderman.teleport"), Sound.Source.AMBIENT, 1f, 1f));
        player.sendActionBar(Component.text("Wooosh!", NamedTextColor.DARK_AQUA));
    }

    private boolean canUseTPStaff(Player player) {
        return player.hasPermission("randompaperutils.use.tpstaff");
    }

    private boolean isTPStaffItem(@Nullable ItemStack item) {
        return item != null
                && item.getType() == Material.BLAZE_ROD
                && item.getItemMeta().getPersistentDataContainer().has(NamespaceKeys.TPSTAFF.get(), PersistentDataType.BYTE);
    }

    private boolean isValidTarget(Material material) {
        return (material.isAir()
                || material == Material.LIGHT
                || material == Material.WATER
                || !material.isSolid()
        ) && !(material == Material.LAVA);
    }
}
