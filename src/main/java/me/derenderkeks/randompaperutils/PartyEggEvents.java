package me.derenderkeks.randompaperutils;

import com.destroystokyo.paper.event.entity.ThrownEggHatchEvent;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDropItemEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class PartyEggEvents implements Listener {
    @EventHandler(ignoreCancelled = true)
    public void chickenEggDropEvent(EntityDropItemEvent event) {
        if (!(event.getEntity() instanceof Chicken)
                || event.getItemDrop().getItemStack().getType() != Material.EGG
                || !isPartyEggEntity(event.getEntity())
        ) {
            return;
        }

        // 5% chance
        if (ThreadLocalRandom.current().nextInt(20) != 0) {
            return;
        }

        event.getItemDrop().setItemStack(PartyEggItem.getItem());
    }

    @EventHandler(ignoreCancelled = true)
    public void partyPotionSplashEvent(PotionSplashEvent event) {
        if (!isPartyPotion(event.getPotion().getItem())) {
            return;
        }
        event.setCancelled(true);
        event.getAffectedEntities().forEach(livingEntity -> {
            if (livingEntity.getType() != EntityType.CHICKEN) {
                return;
            }
            if (livingEntity.getPersistentDataContainer().has(NamespaceKeys.PARTY_EGG.get(), PersistentDataType.BYTE)) {
                return;
            }
            livingEntity.getPersistentDataContainer().set(NamespaceKeys.PARTY_EGG.get(), PersistentDataType.BYTE, (byte) 1);
            spawnRandomFirework(livingEntity.getLocation());
        });
    }

    @EventHandler
    public void partyEggImpactEvent(ThrownEggHatchEvent event) {
        if (!isPartyEgg(event.getEgg().getItem())) {
            return;
        }
        event.setHatching(false);

        final Location location = event.getEgg().getLocation();
        spawnRandomFirework(location);

        (new BukkitRunnable() {
            int counter = 0;

            @Override
            public void run() {
                if (counter >= 60) {
                    this.cancel();
                }
                Location randomLocation = location.clone();
                int maxHorDistance = 16;
                int maxHeight = 10;
                int randX = ThreadLocalRandom.current().nextInt(-maxHorDistance, maxHorDistance);
                int randY = ThreadLocalRandom.current().nextInt(2, maxHeight);
                int randZ = ThreadLocalRandom.current().nextInt(-maxHorDistance, maxHorDistance);
                randomLocation.add(randX, randY, randZ);
                spawnRandomFirework(randomLocation);
                counter++;
            }
        }).runTaskTimer(RandomPaperUtils.getPlugin(RandomPaperUtils.class), 0L, 10L);
    }

    @EventHandler(ignoreCancelled = true)
    public void partyEggDamage(EntityDamageByEntityEvent event) {
        if (((event.getDamager().getType() != EntityType.FIREWORK
                || !isPartyEggEntity(event.getDamager()))
                && (!(event.getDamager() instanceof Egg egg) || !isPartyEgg(egg.getItem())))
        ) {
            return;
        }

        event.setCancelled(true);
    }

    private void spawnRandomFirework(Location location) {
        Firework firework = (Firework) location.getWorld().spawnEntity(location, EntityType.FIREWORK);
        FireworkMeta fireworkMeta = firework.getFireworkMeta();
        FireworkEffect.Builder builder = FireworkEffect.builder();
        ThreadLocalRandom random = ThreadLocalRandom.current();

        builder.flicker(random.nextBoolean());
        builder.withColor(getRandomColor(random));
        if (random.nextInt(3) == 0) {
            builder.withColor(getRandomColor(random));
        }

        List<FireworkEffect.Type> validEffects = List.of(
                FireworkEffect.Type.BURST, FireworkEffect.Type.BALL,
                FireworkEffect.Type.STAR, FireworkEffect.Type.BALL_LARGE
        );
        builder.with(validEffects.get(random.nextInt(validEffects.size())));

        if (random.nextBoolean()) {
            builder.withFade(getRandomColor(random));
        }

        fireworkMeta.addEffect(builder.build());
        fireworkMeta.setPower(0);
        firework.setFireworkMeta(fireworkMeta);
        firework.getPersistentDataContainer().set(NamespaceKeys.PARTY_EGG.get(), PersistentDataType.BYTE, (byte) 1);
        firework.detonate();
    }

    private Color getRandomColor(ThreadLocalRandom random) {
        int rgb = java.awt.Color.HSBtoRGB(random.nextFloat(),
                (float) random.nextInt(30, 100) / 100,
                (float) random.nextInt(50, 100) / 100);
        java.awt.Color color = new java.awt.Color(rgb);
        return Color.fromRGB(color.getRed(), color.getGreen(), color.getBlue());
    }

    private boolean isPartyEgg(ItemStack item) {
        return item != null
                && item.getType() == Material.EGG
                && item.getItemMeta().getPersistentDataContainer().has(NamespaceKeys.PARTY_EGG.get(), PersistentDataType.BYTE);
    }

    private boolean isPartyPotion(ItemStack item) {
        return item != null
                && item.getType() == Material.SPLASH_POTION
                && item.getItemMeta().getPersistentDataContainer().has(NamespaceKeys.PARTY_POTION.get(), PersistentDataType.BYTE);
    }

    private boolean isPartyEggEntity(Entity entity) {
        return entity != null
                && entity.getPersistentDataContainer().has(NamespaceKeys.PARTY_EGG.get(), PersistentDataType.BYTE);
    }
}
