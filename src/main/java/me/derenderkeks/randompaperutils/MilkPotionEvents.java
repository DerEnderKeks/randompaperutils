package me.derenderkeks.randompaperutils;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataType;

public class MilkPotionEvents implements Listener {
    @EventHandler(ignoreCancelled = true)
    public void milkPotionSplashEvent(PotionSplashEvent event) {
        if (!isMilkPotion(event.getPotion().getItem())) {
            return;
        }
        event.setCancelled(true);
        event.getAffectedEntities().forEach(livingEntity ->
                livingEntity.getActivePotionEffects().forEach(potionEffect ->
                        livingEntity.removePotionEffect(potionEffect.getType())));
    }

    private boolean isMilkPotion(ItemStack item) {
        return item != null
                && item.getType() == Material.SPLASH_POTION
                && item.getItemMeta().getPersistentDataContainer().has(NamespaceKeys.MILK_POTION.get(), PersistentDataType.BYTE);
    }
}
